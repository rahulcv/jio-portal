export const APIConstants = {
    "BASE_URL": "https://jio-chat.herokuapp.com/",
    
    //API URL
    "LOGIN_URL": 'api/v1/auth/login',
    "CHAT_URL": 'api/v1/chat/all',
    "SEND_MESSAGE_API": 'api/v1/chat'
}
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { loginService } from '../../services/login.service';
import {APIConstants} from '../../constants/api.constant';// "../../constants/api.constant";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private accountService: loginService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to the api url
        const user = this.accountService.authResponse;
        const isLoggedIn = user; // user && user.token;
        const token = JSON.parse(localStorage.getItem('user'))&&JSON.parse(localStorage.getItem('user')).token?JSON.parse(localStorage.getItem('user')):null;
        const isApiUrl = request.url.startsWith(APIConstants.BASE_URL) && request.url;
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }

        return next.handle(request);
    }
}
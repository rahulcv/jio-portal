import { Component, OnInit } from '@angular/core';
import { loginService } from '../../services/login.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  public visibleSidebar1 = true;
  public loggedIn =  false;

  constructor(private loginService: loginService) { }

  ngOnInit(): void {
    this.loginService.authSource.subscribe(data=> {
      this.loggedIn = data;
      console.log(this.loggedIn);
    });
  }

}

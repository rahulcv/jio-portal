export class LoginModel {
    constructor(
        public email: string,
        public  password: string
    ){}
}

export class LoginResponseModel{
    public token : string
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component"
import { match } from 'minimatch';
import { ChatRoomComponent } from "../app/chat/chat-room/chat-room.component";
import { ChatDialogComponent} from "../app/chat/chat-dialog/chat-dialog.component";
import { AuthGuard } from './utils/auth.guard';

const routes: Routes = [
 // { path:'*', redirectTo: '/login'},
  {path: 'login', component: LoginComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'chatRoom', component: ChatRoomComponent, canActivate: [AuthGuard]},
  {path: 'history', component: ChatDialogComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

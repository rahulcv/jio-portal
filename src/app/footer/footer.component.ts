import { Component, OnInit } from '@angular/core';
import { loginService } from '../../services/login.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public loggedIn: boolean;
  constructor(private loginService: loginService) { }

  ngOnInit(): void {
    this.loginService.authSource.subscribe(data=> {
      this.loggedIn = data;
      console.log(this.loggedIn);
    });
  }

}

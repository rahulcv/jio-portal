import { Component, OnInit } from '@angular/core';
import { APIService } from "../../services/api.service";
import {APIConstants} from "../../constants/api.constant";
import { loginService } from "../../services/login.service";
import { LoginModel, LoginResponseModel } from "../model/login.model";
import {LocalStorageService} from 'ngx-webstorage';  
import {  Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginModel: any;

  constructor(private apiService: APIService, private loginService: loginService, private storage:LocalStorageService, private router: Router) { 
    this.loginModel = new LoginModel('','');
    this.loginService.authResponse = new LoginResponseModel();

    console.log(storage);
  }

  ngOnInit(): void {
  }

  login(loginModel){
    let userCredentials = {
          "email": loginModel.email,
          "password": loginModel.password
    }
    return this.loginService.login(userCredentials).then(response=> {
      console.log(response);
      this.loginService.authResponse =  response;
      if(this.loginService.authResponse && this.loginService.authResponse.token){
        this.loginService.loggedIn = true;
        localStorage.setItem('user', JSON.stringify(response));
        this.loginService.updatedDataSelection(true);

      }
      else{
        this.loginService.loggedIn = false;
        this.loginService.updatedDataSelection(false);
      }
      this.router.navigate(['./dashboard']);
      //this.storage.store('auth','response');
     // this.storage.retrieve('key');
    }, error => {
        console.log(error);
    });
    //return this.apiService.POST(APIConstants.LOGIN_URL,userCredentials);
  }

}

import { Component, OnInit , ViewChild} from '@angular/core';
import {ChatserviceService} from '../chatservice.service';
import {ChatService } from "../../../services/chat.service";
import { ChatModel } from "../../model/chat.model";
import {TableModule} from 'primeng/table';

@Component({
  selector: 'app-chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.scss']
})
export class ChatDialogComponent implements OnInit {
  cols: any;
  chatResponse: any;// new ChatModel();
  rangeDates: Date[];
  loading: boolean;
  @ViewChild('dt') private _table: TableModule;

 

  constructor(private chat : ChatserviceService, private chatService: ChatService) { }

  ngOnInit(): void {
    // this.chat.talk();
    this.getChats();
    this.cols = [
      {fileld: 'user', header: 'user'},
      { field: 'resolvedQuery', header: 'resolvedQuery' },
      { field: 'message', header: 'message' },
      { field: 'text', header: 'text' },      
      // { field: 'speech', header: 'speech' },
  ];
  
  }
  
  getChats(){
    this.loading = true;
    this.chatService.getChats().then(response=> {
      console.log(response);
      this.chatResponse= response;
      this.chatResponse.data.filter(function(item) {
        item.message = item.response.message;
        item.speech = item.response.speech;
        item.resolvedQuery = item.response.resolvedQuery
      })
      this.loading = false;
      console.log(this.chatResponse.data);
    })
  }

  
}

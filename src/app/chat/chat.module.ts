import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChatDialogComponent } from './chat-dialog/chat-dialog.component';
import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { ChatRoomComponent } from './chat-room/chat-room.component';


@NgModule({
  declarations: [ChatDialogComponent, ChatRoomComponent],
  imports: [
    CommonModule,
    TableModule,
    BrowserAnimationsModule,
    CalendarModule,
    FormsModule
  ],
  exports: [ChatDialogComponent, ChatRoomComponent]

})
export class ChatModule { }

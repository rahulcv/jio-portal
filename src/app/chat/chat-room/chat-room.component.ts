import { Component, OnInit } from '@angular/core';
import {ChatService } from "../../../services/chat.service";


@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss']
})
export class ChatRoomComponent implements OnInit {
  public newMessage: any;
  public username: string;
  public messages: any[];

  constructor(private chatService: ChatService) { }

  ngOnInit(): void {
  }

  sendMessage() {
   
    let requestBody = { text: this.newMessage};
    this.chatService.chat(requestBody).then(response => {
        console.log(response);
        this.chatService.storeChatMessages(response);
        this.messages = this.chatService.getChatMessages();
        this.newMessage = "";
        console.log(this.messages);
    })
  }

}

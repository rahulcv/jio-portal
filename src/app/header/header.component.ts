import { Component, OnInit } from '@angular/core';
import { loginService} from "../../services/login.service";
import {  Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedIn: any;
  constructor(private loginService: loginService, private router: Router) { 

  }

  ngOnInit(): void {
    this.loginService.authSource.subscribe(data=> {
      this.loggedIn = data;
      console.log(this.loggedIn);
    });
  }

  logout(){
    this.loginService.updatedDataSelection(false);
    this.router.navigate(['./login'],{replaceUrl: true});
  }
}

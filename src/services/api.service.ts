import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIConstants } from '../constants/api.constant';

@Injectable({
    providedIn: 'root'
})
export class APIService{
    constructor(private httpClient: HttpClient){

    }
    GET(apiUrl){
        return this.httpClient.get(APIConstants.BASE_URL+apiUrl);
    }

    POST(apiUrl, requestBody, httpOptions){
        return this.httpClient.post(APIConstants.BASE_URL+apiUrl, requestBody, httpOptions);
    }

    DELETE(apiUrl, requestBody){
        return this.httpClient.delete(APIConstants.BASE_URL+apiUrl, requestBody);
    }

    PUT(apiUrl, requestBody){
        return this.httpClient.put(APIConstants.BASE_URL+apiUrl, requestBody);
    }
}
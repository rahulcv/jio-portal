import { Injectable } from '@angular/core';
import {APIConstants} from "../constants/api.constant";
import { APIService } from "../services/api.service";
import { loginService } from "../services/login.service";
import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ChatService{   
    public chatMessages = []; 
    constructor(private apiService: APIService, private loginService: loginService){

    }

    getChats(){
        return new Promise((resolve, reject) => {
            this.apiService.GET(APIConstants.CHAT_URL).subscribe(resolve, reject);
        });
    }

    chat(requestBody){
        var headers_object = new HttpHeaders();
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this.loginService.authResponse.token);
       
      
        const httpOptions = {
        headers: headers_object
        };
        return new Promise((resolve, reject) => {
            this.apiService.POST(APIConstants.SEND_MESSAGE_API,requestBody, httpOptions).subscribe(resolve, reject);
        });
    }

    storeChatMessages(message) {
        this.chatMessages.push(message.data);
    }

    getChatMessages(){
        return this.chatMessages;
    }
}
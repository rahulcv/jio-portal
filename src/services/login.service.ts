import { Injectable } from '@angular/core';
import {APIConstants} from "../constants/api.constant";
import { APIService } from "../services/api.service";
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class loginService{
    public authResponse: any;
    public loggedIn: boolean;
    public authSource = new BehaviorSubject(JSON.parse(localStorage.getItem('user')));
    public authChanges = this.authSource.asObservable();
    constructor(private apiService: APIService){

    }

    login(userCredentials){
        return new Promise((resolve, reject) => {
            this.apiService.POST(APIConstants.LOGIN_URL, userCredentials, '').subscribe(resolve, reject);
        });
    }
    logout(){
      localStorage.removeItem('user');
      this.updatedDataSelection(null);
    }

    updatedDataSelection(data){
      this.loggedIn = data;
        this.authSource.next(data);
      }

}